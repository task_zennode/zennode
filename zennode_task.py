products = {"Product A": 20, "Product B": 40, "Product C": 50} #products 

quantity_A = int(input("Enter the quantity of Product A: "))  
quantity_B = int(input("Enter the quantity of Product B: "))   #quantity selecting
quantity_C = int(input("Enter the quantity of Product C: "))

gift_wrap_A = input("Product A -  wrapped as a gift? (y/n): ").lower() == "y"
gift_wrap_B = input("Product B -  wrapped as a gift? (y/n): ").lower() == "y"   #gift wrap (y or n)
gift_wrap_C = input("Product C -  wrapped as a gift? (y/n): ").lower() == "y"

total_product_A = quantity_A * products["Product A"]
total_product_B = quantity_B * products["Product B"]   #total rate of each products
total_product_C = quantity_C * products["Product C"]

subtotals = [total_product_A, total_product_B, total_product_C]
total_cart = sum(subtotals)     #sum of total cart products
 
discounts = []
discount_amounts = []        

if total_cart > 200:                       #different conditions and append the discounts to discounts and discount- 
    discounts.append("flat_10_discount")    #-amount lists and find the maximum in the discounts list
    discount_amounts.append(10)

for i, quantity in enumerate([quantity_A, quantity_B, quantity_C]):
    if quantity > 10:
        discount = subtotals[i] * 0.05
        discounts.append("bulk_5_discount")
        discount_amounts.append(discount)

total_quantity = quantity_A + quantity_B + quantity_C

if total_quantity > 20:
    discount = total_cart * 0.1
    discounts.append("bulk_10_discount")
    discount_amounts.append(discount)

discount_tiered_50 = 0

for i, quantity in enumerate([quantity_A, quantity_B, quantity_C]):
    if quantity > 15 and total_quantity > 30:
        quantity_below_15 = min(quantity, 15)
        quantity_above_15 = max(quantity - 15, 0)
        product_price = products[list(products.keys())[i]]
        discount = quantity_above_15 * 0.5 * product_price
        discounts.append("tiered_50_discount")
        discount_amounts.append(discount)

max_discount_amount = max(discount_amounts)
max_discount_index = discount_amounts.index(max_discount_amount)
discount_applied = discounts[max_discount_index]
print(max_discount_amount)
total_cart -= max_discount_amount

gift_wrap_total = (gift_wrap_A * quantity_A) + (gift_wrap_B * quantity_B) + (gift_wrap_C * quantity_C)
shipping_fee = total_quantity // 10
if total_quantity % 10 != 0: 
    shipping_fee += 1

total = total_cart + gift_wrap_total + (shipping_fee * 5)  #find the total 

print("\n_______________________________________________________")
print("\n")
for i, quantity in enumerate([quantity_A, quantity_B, quantity_C]):
    product_name = list(products.keys())[i]
    product_price = products[product_name]
    subtotal = subtotals[i]
    print(f"product name is {product_name} , Quantity: {quantity} and  total amount: ${subtotal}")

print(f"\nSubtotal: ${sum(subtotals)}")
if discount_applied:
    print(f"discount applied = {discount_applied} |****| discount amount = ${max_discount_amount}")
print(f"Shipping Fee =  ${shipping_fee * 5} and Gift Wrap Fee: ${gift_wrap_total}")
print(f"\nTotal: ${total}")

print("\n_______________________________________________________")
